﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace common.components
{
    public class StringHelper
    {
        public static string ConvertToUppercase(string text)
        {
            return text.ToLower();
        }
        public static string ConvertToLower(string text)
        {
            return text.ToLower();
        }
    }
}
